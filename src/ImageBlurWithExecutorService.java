/**
 * Created by user on 10.11.2016.
 */
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;
public class ImageBlurWithExecutorService {
    private static int imgSize;
    //Число пикселей в маске (13 - значит справа и слева по 6)
    private static int mBlurWidth = 13;

    public static void main(String[] args) {
        try {
            System.out.println("Loading image...");

            BufferedImage lImg = ImageIO.read(new File("D:\\HomeWork14\\src\\cat.jpg"));
            System.out.println("Image loaded.");

            imgSize = lImg.getWidth();

            System.out.println("Processing image...");
            BufferedImage rImg = process(lImg);
            System.out.println("Image processed");

            System.out.println("Saving image...");
            ImageIO.write(rImg, "jpg", new File("D:\\HomeWork14\\src\\out.jpg"));
            System.out.println("Image saved");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private static BufferedImage process(BufferedImage lImg) throws ExecutionException, InterruptedException {
        //представляем изображение, как массив пикселей ()
        int[] rgb = imgConvertRgb(lImg);

        long start = System.currentTimeMillis();
        //задаем число потоков и задач
        int[] transformed = blurParallel(rgb, 4, 20);
        long end = System.currentTimeMillis();

        System.out.println((end - start) + "ms");

        //переводим массив пикселей в изображение
        return rgbConvertImg(transformed);
    }

    //Consumer just for getting Future value avoiding exceptions
    private static Consumer futureConsume  = future -> {
        try {
            ((Future)future).get();
        } catch (Exception e) {
        }};

    private static int[] blurParallel(int[] rgb, int threadsCount, int tasksCount) throws ExecutionException, InterruptedException {

        //результирующий массив
        final int[] rgbRes = new int[rgb.length];
        final int partSize = rgb.length / tasksCount;

        ExecutorService service = Executors.newWorkStealingPool(4);

        Collection<Future> resultList = new ArrayList<>();

        for (int i = 0; i < tasksCount; i++) {
            final int k = i;
            resultList.add(service.submit(() -> computeDirectly(rgb, k * partSize, (k + 1) * partSize, rgbRes)));
        }

        resultList.stream().forEach(futureConsume);

        service.shutdown();

        return rgbRes;
    }

    static protected void computeDirectly(int[] source, int start, int length, int[] adjacent) {

        for (int i = start; i < start + length; i++) {
            List<Integer> adjacentPixels = new ArrayList<>();
            int row = i / imgSize;
            int col = i % imgSize;
            int mbw = mBlurWidth / 2;

            for (int nRow = row - mbw > 0 ? row - mbw : 0; nRow < (row + mbw > imgSize ? imgSize : row + mbw); nRow++) {
                int[] subArray = Arrays.copyOfRange(source, nRow * imgSize + (col - mbw > 0 ? col - mbw : 0), nRow * imgSize + (col + mbw > imgSize - 1 ? imgSize - 1: col + mbw));
                Arrays.stream(subArray).forEach(num -> adjacentPixels.add(num));
            }
            adjacent[i] = processPixel(adjacentPixels);
        }
    }

    private static int processPixel(List<Integer> adjacentPixels) {
        int redComponent = 0;
        int greenComponent = 0;
        int blueComponent = 0;
        for (Integer pixel :adjacentPixels) {
            redComponent += (pixel & 0x00_ff_00_00) >> 16;
            greenComponent += (pixel & 0x00_00_ff_00) >> 8;
            blueComponent += pixel & 0x00_00_00_ff;

        }
        int resultRed = redComponent / adjacentPixels.size();
        int resultGreen = greenComponent / adjacentPixels.size();
        int resultBlue = blueComponent / adjacentPixels.size();


        return (resultRed << 16) + (resultGreen << 8) + resultBlue;
    }

    private static int[] imgConvertRgb(BufferedImage img) {
        int[] result = new int[imgSize * imgSize];
        img.getRGB(0, 0, imgSize, imgSize, result, 0, imgSize);
        return result;
    }

    private static BufferedImage rgbConvertImg(int[] rgb) {
        BufferedImage result = new BufferedImage(imgSize, imgSize, BufferedImage.TYPE_INT_RGB);
        result.setRGB(0, 0, imgSize, imgSize, rgb, 0, imgSize);
        return result;
    }
}


